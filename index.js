// console.log("Hello world");

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript.
// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.);

// New features to JavaScript

console.log("==================");

console.log("ES6 Updates");
console.log("=> Exponent Operator");

// using Math Object Methods
const firstNum = Math.pow(8, 3); /*8*8*8*/
console.log(firstNum);

// using the exponent operator
const secondNum = 8 ** 3; /* 8^3 */
console.log(secondNum);

// [SECTION] Template Literals

/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("=> Template Literals");

let name = "John"

// Pre-template Literals concat
let message = "Hello " + name + "! Welcome to programming";
console.log(message);

// String using template literals
// Uses backtincks(``) instead of ("") or ('')
 message = `Hello ${name}! Welcome to programming!`;
 console.log("Message with template literals");
 console.log(message); 


 // It allows us to name array elements with variableNames instead of using the index numbers.
 /*
 	- Syntax:
 		let/const [variableName1, variableName2, variableName3] = arrayName;
 */

 console.log("=========================");
 console.log("Array Destructuring");

 const fullName = ["Juan", "Dela", "Cruz"];

 // Pre-array Destructuring
 console.log(fullName[0]);
 console.log(fullName[1]);
 console.log(fullName[2]);

 console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);

 
 // Array Descructuring
 // variable naming for array destructuring is based on the developers choice.

 const [firstName, middleName, lastName] = fullName;

 console.log(firstName);
 console.log(middleName);
 console.log(lastName);

 console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`);


 // [SECTION] Object Destructuring
 /*
 	- Shortens the syntax for accessing properties form object
 	- The difference with array destructuring, this should be exact property name.
 	-Syntax:
 		let/const {propertyNameA, propertyNameB, propertyNameC} = object;

 */

 console.log("----------");
 console.log("=> Object Destructuring");

 const person = {
 	givenName: "Jane",
 	mName: "Dela",
 	surName: "Cruz"
 }

 //Pre-object destructuring
 console.log(person.givenName);
 console.log(person.middleName);
 console.log(person.surName);

 console.log(`Hello ${person.givenName} ${person.middleName} ${person.SurName}! It's good to see you again`);

console.log("=======================");

// Object Destructuring
const {givenName, mName, surName} = person;

console.log(givenName);
console.log(mName);
console.log(surName)

console.log(`Hello ${givenName} ${mName} ${surName}! It's good to see you again`);

console.log("-----------------");
console.log("=> Arrow Functions");

// SECTION Arrow Functions

/*
	-Compact alternative syntax to traditional fucntions
	-Useful for code snippers where creating functions will not be reused in any other portion of code.
	-This will work with "function expression." (s17)

	example of funcExpression

	let funcExpression = fucntion funcName(){
		console.log("Hello from the other side");
	}
	funcExpression();

*/
/*
	syntax 

	let/const variableName = (parameter) => {
	
	code to execute

	invocation
	variableName(argument);
	}
*/

const hello = () => {
	console.log("Hello from the other side");
}

hello();

const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`);
});

console.log("======================================")

// forEach method with the use of arrow function
/*
	syntax:

	arrayName.arrayMethod((parameter) =>
		code to execute;
	);

*/
students.forEach((student) =>
	console.log(`${student} is a student`));

// anonymous fucntion - a function that has no function name

console.log("+========================");

console.log("=> Implicit return using arrow fucntions");

// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;

// const add = (x, y) =>{
// 				x + y;
// }
/*
	syntax

			let/const variableName = (parameter/s) => code to execute;
*/
const add = (x,y) => x + y;

let total = add(1,2);
console.log(total);


/*SECTION default function argument value*/
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning ${name}`;
console.log(greet());
console.log(greet("John"));

console.log("-----------");
console.log("=> Class-Based Object Blueprint: ");
// [SECTION] Class-Based Object Blueprint
// Another approach in creating an object with key and value;
// Allows creation/instantiation of object using classes as blueprints.

// The constructor is a special method of a class for creating object for the class.

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

/*New operator creates/initiates a new object with the given argument as value of its property*/

const myCar = new Car();
console.log(mycar);

// Reassigning value of each property
// Mini activity

/*
	1. Reassign using dot notation brand property with a value ford
	2. Reassign using dot notaion name property with a value Ranger raptor
	3. Reassign using dor notation yaer with a value of 2021
	4. Console log or display the object myCar

*/

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);